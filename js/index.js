
$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
    interval: 2000
    });
    $('#contacto').on('show.bs.modal	',function(e){
        console.log('el modal se está mostrando');
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled',true);          
    });
    $('#contacto').on('shown.bs.modal',function(e){
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal',function(e){
    console.log('Se cierra el modal');
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log('El modal se mostró');
        $('#contactoBtn').prop('disabled',false);
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-secondary');

    });
});        